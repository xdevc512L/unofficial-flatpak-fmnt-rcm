#!/bin/bash

# The program hardcodes the path to find the firefox profiles
FIREFOX_DIR=${FIREFOX_DIR:-"$HOME/.var/app/org.mozilla.firefox/.mozilla"}
if [[ -f $FIREFOX_FLATPAK ]]; then
  ln -sf "$FIREFOX_FLATPAK" "$HOME/.mozilla"
fi
CLASSPATH="${CLASSPATH}:/app/lib/*"
JAVA_USER_ROOT=${XDG_CONFIG_HOME:-$HOME/.config}/java
/app/jre/bin/java -Djava.util.prefs.userRoot="${JAVA_USER_ROOT}" -classpath "${CLASSPATH}" es.gob.fnmt.cert.certrequest.CertRequest "$@" 
