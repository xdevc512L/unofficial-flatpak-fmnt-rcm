# An unofficial Flatpak package for the 'Configurador FNMT-RCM' of Spanish government
## Status

- The program starts fine and works as expected for processing requests.
- Certificate installation works on Firefox Flatpak but it's hardcoded by the program. See
- No Chrome support atm (though MRs are welcomed)

# Why don't you publish it to Flathub?

Personally I would disencourage the usage of this program, since it's pretty much legacy software at this point (specially if you can make use of the [Android App](https://play.google.com/store/apps/details?id=es.fnmtrcm.ceres.certificadoDigitalFNMT)).

## Build and Install
See the [Justfile](./Justfile)

## Other Firefox-based browsers
You can specify it with the `$FIREFOX_DIR` (note that flatpak will also need access to it).
Here is an example using [librewolf](https://librewolf.net/):
```sh
FIREFOX_DIR=~/.var/app/io.gitlab.librewolf-community/.librewolf
flatpak run --filesystem=$FIREFOX_DIR --env=FIREFOX_DIR=$FIREFOX_DIR es.gob.fnmt.sede.ConfiguradorFNMT 
``` 
